# Maven 연습 메모

## Maven Java 프로젝트 생성

콘솔에 다음 명령어를 입력

```cmd
# Windows
> mvn archetype:generate ^
-DgroupId=io.gitlab.kakaruu ^
-DartifactId=javaprj ^
-DarchetypeArtifactId=maven-archetype-quickstart
```

```bash
# Linux
$ mvn archetype:generate \
-DgroupId=io.gitlab.kakaruu \
-DartifactId=javaprj \
-DarchetypeArtifactId=maven-archetype-quickstart
```

이미 만들어져 있는 `maven-archetype-quickstart`라는 `템플릿 프로젝트(archetype)`를 기반으로 `javaprj`라는 나만의 템플릿 프로젝트를 `생성(generate)`하겠다는 의미이다.</br>
`maven-archetype-quickstart`는 아주 자바 개발할 때 사용되는 기본적인 템플릿이다.</br>
원한다면 이렇게 생성한 `javaprj`을 배포해서 다른 사람이 참조할 수 있게 할수도 있다.

## 프로젝트 빌드

소스를 컴파일하기 위해서 콘솔에 다음 명령을 입력한다.

```cmd
> mvn compile
...
[ERROR] Source option 5 is no longer supported. Use 6 or later.
[ERROR] Target option 1.5 is no longer supported. Use 1.6 or later.
...
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.1:compile (default-compile) on project maven-practice: Compilation failure: Compilation failure:
...
```

실행하면 자바 자바 5버전은 더이상 지원하지 않으니 6버전 이상을 사용하라는 메시지가 표기된다.</br>
pom.xml을 보면 기본적으로 build에 대한 어떠한 설정도 되어있지 않는것을 볼 수 있다.</br>
아무런 설정이 없기 때문에 `default-compile`인 maven-compiler-plugin:3.1를 사용한 것 같은데, 이 컴파일러가 5버전 이하는 지원하지 않는듯 하다.</br>
그런데 java source와 target에 대한 설정이 없는데 왜 버전 5가 기본적으로 사용되었는지는 잘 모르겠다.</br>

### pom에 사용할 자바 버전 설정하기

프로퍼티에 `maven.compiler.target`과 `maven.compiler.source`에 버전을 설정해주니 메이븐 컴파일러가 해당 버전으로 빌드를 정상적으로 수행했다.</br>
아마 컴파일러가 기본적으로 위의 프로퍼티를 참조하는 듯 하다.</br>
pom에서 `build>plugins`에다가 `maven-compiler-plugin`의 버전과 자바 타겟 버전을 설정할 수 있는 것으로 아는데, 프로퍼티로도 간접적인 설정이 가능한가보다.

### 컴파일된 파일이 들어가는 곳

컴파일된 파일들은 target 폴더에 들어간다.

### 패키지 빌드하기

```cmd
> mvn package
```

pom의 `packaging`에 설정해놓은 방식으로 프로젝트를 패키징한다.\
package 명령을 수행하면 compile도 함께 수행된다.

### 테스트 빌드하기

```cmd
> mvn test
```

뭔가 빌드가 되는데 junit을 공부해야 이해할 수 있지 않을까 싶다.

### 결과물 청소하기

```cmd
> mvn clean
```

`target` 폴더에 생성된 결과물들을 청소할 수 있다.

### jar 패키지 실행하기

```cmd
> java -cp maven-practice-1.0-SNAPSHOT.jar io.gitlab.kakaruu.App
```

`cp`는 `classpath`를 의미한다.\
jar파일에 있는 `io.gitlab.kakaruu.App`의 main을 실행하라는 의미

### class 파일 실행하기

```cmd
# classes 폴더에서
> java io.gitlab.kakaruu.App
# or
> java io/gitlab/kakaruu/App

# 다른 위치에서
> java -cp /path/of/classes io.gitlab.kakaruu.App
# or
> java -cp /path/of/classes io/gitlab/kakaruu/App
```

## 메이븐 수행단계(Phases)/=명령어/=?생명주기(Lifecycle)

메이븐의 Phases는 세분화하면 너무 많기 때문에 간략하면 다음과 같다.\
validate - **`compile`** - **`test`** - **`package`** - verify - **`install`** - **`deploy`**\
강조 표기해놓은 페이즈는 명령어로 수행할 수 있는 페이즈들로, 명령어를 실행하면 앞의 작업들을 쭉 실행하면서 해당하는 페이즈까지 수행하고 멈추게 된다.\
세부적인 페이즈들은 패키징 방법에 따라 달라진다.

### Plugins

메이븐의 각 페이즈들은 자신들을 담당하는 프로그램들에 의해서 수행되는데, 이 프로그램들을 메이븐에서는 `Plugin`이라고 부른다.\
플러그인이라는 이름에서 알 수 있듯이, `개발자는 필요에 따라 플러그인을 뺏다 꽂을 수 있고, 원하는 플러그인으로 교체할수도 있다.`\
예를들어서 패키지 방식이 jar일경우, default로 validate에는 플러그인이 설정되어 있지 않고, compile에는 maven-compiler-plugin이 설정되어 있는데, 개발자는 pom.xml에서 자신이 원하는 플러그인으로 교체할 수 있다.

### Goal

`플러그인을 또 내부적으로 구성하고 있는 프로그램들, 작은 절차들`이 있는데 이러한 작은 절차들을 Goal이라고 부른다.

### 명령어를 수행하기 위해 필요한 Plugin 정보 보는 방법

아래의 명령어를 입력하면 현재 설정되어있는 플러그인 정보를 확인할 수 있다.

```cmd
> mvn help:describe -Dcmd=compile
...
It is a part of the lifecycle for the POM packaging 'jar'. This lifecycle includes the following phases:
* validate: Not defined
* initialize: Not defined
* compile: org.apache.maven.plugins:maven-compiler-plugin:3.1:compile
* process-classes: Not defined
* generate-test-sources: Not defined
* process-test-sources: Not defined
* generate-test-resources: Not defined
* process-test-resources: org.apache.maven.plugins:maven-resources-plugin:2.6:testResources
* test-compile: org.apache.maven.plugins:maven-compiler-plugin:3.1:testCompile
* process-test-classes: Not defined
* test: org.apache.maven.plugins:maven-surefire-plugin:2.12.4:test
* prepare-package: Not defined
* package: org.apache.maven.plugins:maven-jar-plugin:2.4:jar
* pre-integration-test: Not defined
* integration-test: Not defined
* post-integration-test: Not defined
* verify: Not defined
* install: org.apache.maven.plugins:maven-install-plugin:2.4:install
* deploy: org.apache.maven.plugins:maven-deploy-plugin:2.7:deploy
...
```

### 메이븐이 기본적으로 제공하는 플러그인에 대한 문서

https://maven.apache.org/plugins/index.html#

## 메이븐 프로젝트를 이클립스에 Import하기

Eclipse -> File -> Import를 눌러 Import 창을 연다.\
`File System` | `Projects from Folder or Archive` | `Existing Maven Projects` 셋 중 하나 아무거나 선택해서 Maven 프로젝트를 Import 해주면 된다.\
만약 Maven 프로젝트를 인식하지 못 하는 경우, 이클립스에 `Maven Integration` 플러그인이 제대로 설치되어 있는지 확인해보자.\
이클립스는 pom.xml파일을 읽어서 프로젝트 속성을 보여주기 위해 자동으로 .classpath 파일을 생성한다.\
pom.xml 파일이 변경될 경우 프로젝트를 우클릭하고 Maven -> Update Project를 눌러주면 .classpath 파일의 내용이 갱신된다.

## Compile 플러그인 설정해보기

[프로젝트 빌드](#프로젝트-빌드)에서 설정했었던 `maven.compiler.source`와 `maven.compiler.target` 프로퍼티를 제거하자.

> 어차피 기본 플러그인인 `maven-compiler-plugin`를 사용할거라면 위의 프로퍼티로 설정해도 문제 없긴하지만, 플러그인을 다른 것을 사용하고 싶을수도 있기 때문에, 명시적으로 설정하는 편이 더 세련된 방법인 것 같다.

build>plugins에 아래와 같이 `maven-compiler-plugin`을 설정한다.

```xml
<build>
    <plugins>
      <!-- maven 그룹이 제공하고 있는 compiler 플러그인을 설정한다. -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
          <source>11</source>
          <target>11</target>
        </configuration>
      </plugin>
    </plugins>
  </build>
```

## Maven 기본 프로젝트를 웹 프로젝트로 변경하기

IDE에서 기본 제공하는 Java 프로젝트로 소스를 작성하다가 웹 프로그램으로 바꾸려면 설정을 건드려야하는 부분이 한두가지가 아니다.\
하지만 Maven 프로젝트와 이클립스를 사용하면 pom.xml에서 `packaging`을 `war`로 변경하기만 하는 것으로 쉽게 Java 프로젝트에서 웹 프로젝트로 변경할 수 있다.

### 자동으로 변경하는 방법

- pom.xml에서 `jar`로 설정되어 있는 `packaging`을 `war`로 변경한다.
- 프로젝트를 우클릭하고 Maven -> Update Project를 클릭한다.
- src/main 폴더에 `webapp`이라는 폴더가 생성된다.
- `webapp` 폴더 안에 `WEB-INF` 폴더를 생성하고, `web.xml` 파일을 추가해준다.
- `web.xml` 파일은 `apache tomcat의 webapps/ROOT/WEB-INF 폴더` 안에 있는 것을 가져오거나 직접 작성하면 된다.

`Upate Project` 버튼을 누르지 않고 웹 프로젝트로 변경하는 방법은 [이 글](https://stackoverflow.com/questions/16142088/change-maven-archetype-after-a-project-is-created-in-eclipse)에 답변으로 적혀있다.(근데 이거도 이클립스의 `Dynamic Web Module`이라는 Facets을 통해 구조를 생성하는 것 같다.)

### Servlet, JSP 등을 위한 라이브러리 추가하기

`Build Path`에 `Server Runtime을 추가`해서 필요한 라이브러리들을 참조하는 방법으로 간단하게 해결할 수 있긴하다.\
하지만 `Build Path`는 `IDE에 종속된 설정`이기 때문에 다른 IDE를 쓰거나 다른 PC에서 프로젝트를 가져오는 등의 상황에서 다시 라이브러리들을 설정해주어야 한다.\
따라서 pom.xml에 라이브러리들을 설정하는 방식이 바람직하다.

#### API의 버전을 알아내서 추가하는 방법(비추)

- 우선 사용할 아파치 톰캣에 맞는 jsp 버전을 확인한다.
  - [9버전 문서](https://tomcat.apache.org/tomcat-9.0-doc/index.html). 좌측의 Reference를 확인하면 jsp 버전을 확인할 수 있다.
- pom에 dependency 추가
  - 방법 1: Maven Repository에서 `javax.servlet.jsp >> jsp-api`를 찾아서 dependency 설정 문구를 복사 붙여넣기 한다.
  - 방법 2: 프로젝트 우클릭 -> Maven -> Add Dependency를 클릭해서 `jsp-api`를 검색해서 추가한다.

#### 톰캣 버전으로 라이브러리를 추가하는 방법

위의 방법에서 `jsp-api` 대신 `tomcat-jsp-api`를 찾아서 입력해주면 된다.

## Cargo로 웹서버 실행하기

Cargo는 다양한 자바 EE 어플리케이션 컨테이너들을 실행할 수 있는 wrapper 라고한다.\
정확히 무슨 의민지는 잘 모르겠지만 maven-wrapper 같은게 돌아가는 모양새로 유추해보면, 사용자에게 필요한 실행환경(컨테이너=WAS같은거)을 자동으로 다운로드하고, 다운로드한 컨테이너로 어플리케이션을 감싼(wrapping) 뒤에 실행하는 뭐.. 그런 프로그램이라는 의미인 것 같다.\
약간.. Docker의 하위호환의 개념 느낌?\
원래는 `Embed Tomcat`으로 웹 프로그램을 실행해보려다가 어쩌다보니 Cargo를 알게 되어서 써보게 되었다.

- [Cargo 공식 사이트](https://codehaus-cargo.github.io/cargo/Home.html)
- [Cargo 바로 실행 관련 명령어들](https://codehaus-cargo.github.io/cargo/Maven+3+Plugin.html)
  - `Very quick start`에 나열된 명령어는 plugin 설정 없이 그냥 바로 실행할 수 있는 명령어들이 나열되어 있다.
  - `The Cargo Maven plugin in detail`에는 plugin 설정 후 사용할 수 있는 명령어들이 나열되어 있다.
- [Maven의 pom.xml에 Cargo를 설정하기 위한 가이드](https://codehaus-cargo.github.io/cargo/Maven+3+Plugin+Reference+Guide.html)
  - plugin에 설정할 수 있는 태그들이 나와있다.
  - container는 어플리케이션을 실행할 컨테이너의 정보를 입력한다.
    - 지원하는 컨테이너는 사이트 좌측의 Containers에서 확인할 수 있다.
    - 컨테이너를 다운로드해서 사용할 것인지, 이미 설치된 컨테이너를 사용할 것인지, embedded 방식으로 사용할 것인지를 선택할 수 있다.
  - 각 항목을 클릭하면 어떤 속성값들을 설정할 수 있는지 설명되어 있다.

### pom.xml의 plugin 설정

```xml
<plugin>
  <groupId>org.codehaus.cargo</groupId>
  <artifactId>cargo-maven3-plugin</artifactId>
  <!-- 왠진 잘 모르겠지만 1.9.9로는 tomcat9x가 동작하지 않는다. -->
  <version>1.9.8</version>
  <configuration>
    <container>
      <containerId>tomcat9x</containerId>
      <type>embedded</type>
    </container>
    <configuration>
      <properties>
        <cargo.servlet.port>8080</cargo.servlet.port>
      </properties>
    </configuration>
  </configuration>
</plugin>
```

위와 같이 plugin을 설정하면 `tomcat9`를 `embedded` 방식으로 가져와서 빌드된 웹 어플리케이션을 실행해 볼 수 있다.\
pom.xml에 플러그인을 설정한 뒤에, 아래 명령어를 실행해보자.

```cmd
> mvn clean verify cargo:run
```

이클립스 처럼 자동 빌드, 재시작이 안되므로 clean, verify를 선행해야 한다.\
embedded 타입의 컨테이너는 `프로젝트 Root/target/cargo`에 생성된다.
